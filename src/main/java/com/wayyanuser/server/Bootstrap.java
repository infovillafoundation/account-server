package com.wayyanuser.server;

import com.wayyanuser.server.spark.GetAccountsRoute;
import com.wayyanuser.server.spark.PostAccountRoute;
import spark.Spark;

public class Bootstrap {

    public static void main(String[] args) {
        Spark.setPort(getHerokuAssignedPort());
        Spark.post(new PostAccountRoute("/accounts"));
        Spark.get(new GetAccountsRoute("/accounts"));
    }

    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }

}
