package com.wayyanuser.server.spark;

import com.avaje.ebean.Ebean;
import com.wayyanuser.server.Account;
import com.wayyanuser.server.JsonTransformer;
import spark.Request;
import spark.Response;

/**
 * Created by aztunwin on 18/12/15.
 */
public class GetAccountsRoute extends JsonTransformer {
    public GetAccountsRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        System.out.println(Ebean.find(Account.class).findList().size());
        return Ebean.find(Account.class).findList();
    }
}
