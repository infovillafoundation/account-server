package com.wayyanuser.server.spark;

import com.avaje.ebean.Ebean;
import com.wayyanuser.server.Account;
import com.wayyanuser.server.JsonTransformer;
import spark.Request;
import spark.Response;

import java.io.IOException;

/**
 * Created by aztunwin on 18/12/15.
 */
public class PostAccountRoute extends JsonTransformer {
    public PostAccountRoute(String path) {
        super(path);
    }

    @Override
    public Object handle(Request request, Response response) {
        System.out.println("handling: " + request.body());
        Account account = null;
        try {
            account = mapper.readValue(request.body(), Account.class);
        } catch (IOException e) {
            response.status(500); // Server-side error
            System.out.println("error: " + e.getMessage());
            return createErrorResponse("Account couldn't be saved");
        }
        System.out.println("saving: ");
        Ebean.save(account);
        response.status(201); // 201 Created
        return account;
    }

}
