package com.wayyanuser.server;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by aztunwin on 18/12/15.
 */

@Entity
public class Account {
    @Id
    private long id;
    @Column(length = 20, nullable = false, unique = true)
    private String username;
    @Column(length = 20, nullable = false, unique = false)
    private String password;

    public Account() {

    }

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
