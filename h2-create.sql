create table account (
  id                        bigint not null,
  username                  varchar(20) not null,
  password                  varchar(20) not null,
  constraint uq_account_username unique (username),
  constraint pk_account primary key (id))
;

create sequence account_seq;



